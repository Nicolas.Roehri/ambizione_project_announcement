# Ambizione project announcement repo

Repository to store all files related to job annoucements for the Swiss National Science Foundation (SNSF) Ambizione project titled “Network Analysis to Predict Eligibility for Epilepsy Surgery”.

## Ambizione project summary

### Network Analysis to Predict Eligibility for Epilepsy Surgery

One percent of the world’s population suffers from epilepsy, one-third of whom have a drug-resistant form. Epilepsy surgery, which consists in resecting the part of the brain involved in generating seizures (i.e., the epileptogenic zone, EZ), is the most effective treatment option to achieve seizure freedom in these patients. About thirty percent of patients undergoing surgery are not seizure-free. This high rate of surgical failure has motivated the search for electrophysiological biomarkers to better localise the EZ or predict the success of the surgery.  
Epilepsy is considered a disorder of neural networks, involving multiple cortical and subcortical regions. It is therefore natural to study the interactions between these brain regions. The epileptic network can be estimated and characterised using several tools, among them high-density EEG (hd-EEG), magnetoencephalography (MEG), and in some patients, intracerebral EEG. A growing body of evidence has shown that increased connectivity of this network was linked with the severity of the disease and bad surgical outcomes. Network analysis, therefore, appears to be an ideal biomarker to aid surgical decision-making.  
In this project, we will first validate network metrics derived from hd-EEG and MEG by comparing them to those obtained with simultaneously recorded intracranial EEG. The second objective is to assess the added prognostic value of network measures based on hd-EEG/MEG in a large multicentre clinical population of over 200 patients with focal epilepsy and train a machine learning algorithm to help the surgical decision-making. This work has a potentially high impact on the clinical routine of epilepsy surgery and the subsequent methodological advances could be applied to other fields of neuroscience. All the code and some data will be made openly available.
